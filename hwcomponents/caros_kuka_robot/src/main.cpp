#include <caros/kuka_robots.h>

#include <caros/common_robwork.h>

#include <rw/loaders/WorkCellLoader.hpp>

#include <ros/ros.h>

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "caros_kuka_lwr");

  ros::NodeHandle nh("~");

  caros::KukaRobots lwr(nh);
  lwr.start();

  return 0;
}
