#pragma once
#include <cstdlib>
#include <deque>
#include <iostream>
#include <thread>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <fstream>
#include <string>
#include <rw/math.hpp>
#include <ros/ros.h>
#include <geometry_msgs/WrenchStamped.h>
#include <unistd.h>

void default_set_state(std::string, void*);

typedef void (*input_handler)(std::string, void*);

class tcp_client
{
private:
    enum { header_length = 4, max_msg_lenght = 2048 };
    boost::asio::io_service& io_service_;
    boost::asio::ip::tcp::socket socket_;
    boost::asio::streambuf buffer_;
    std::string read_msg_;
    char msg_len[tcp_client::header_length];
    char msg_txt[tcp_client::max_msg_lenght];
    int body_length_;
    std::deque<std::string> write_msgs_;
    bool running_;
    std::string terminate_connection_string;
    input_handler input_handle;
    void *v_state;
public:
    tcp_client(boost::asio::io_service& io_service, boost::asio::ip::tcp::resolver::iterator endpoint_iterator, input_handler function = default_set_state, void *def_state=nullptr);
    ~tcp_client();
    void set_terminate_connection_string(std::string new_msg);
    bool running() { return running_; } 
    
    void write(const std::string& msg);
    
    void close();
private:
    void do_connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator);
    void do_read_header();
    void do_read_body();
    void do_write();
};
