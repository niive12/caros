#include <caros/serial_device_action_interface.h>
#include <caros/common.h>
#include <caros/common_robwork.h>

#include <rw/math.hpp>

#include <vector>
#include <stdexcept>
#include <tuple>

using namespace caros;

SerialDeviceActionInterface::SerialDeviceActionInterface(ros::NodeHandle nh)
    : nh_(nh, SERIAL_DEVICE_ACTION_INTERFACE_SUB_NAMESPACE),
      as_move_ptp_(nh_, "move_ptp", boost::bind(&SerialDeviceActionInterface::movePtpCB, this, _1),
                   ACTION_SERVER_NO_AUTOSTART),
      as_move_lin_(nh_, "move_lin", boost::bind(&SerialDeviceActionInterface::moveLinCB, this, _1),
                   ACTION_SERVER_NO_AUTOSTART),
      as_move_vel_(nh_, "move_vel", boost::bind(&SerialDeviceActionInterface::moveVelCB, this, _1),
                   ACTION_SERVER_NO_AUTOSTART),
      as_move_servo_ptp_(nh_, "move_servo_ptp", boost::bind(&SerialDeviceActionInterface::moveServoPtpCB, this, _1),
                         ACTION_SERVER_NO_AUTOSTART)
{
  /* Start the action servers */
  as_move_ptp_.start();
  ROS_INFO("move_ptp successfully initialized! Ready for action...");
  as_move_lin_.start();
  ROS_INFO("move_lin successfully initialized! Ready for action...");
  as_move_vel_.start();
  ROS_INFO("move_vel successfully initialized! Ready for action...");
  as_move_servo_ptp_.start();
  ROS_INFO("move_servo_ptp successfully initialized! Ready for action...");
}

SerialDeviceActionInterface::~SerialDeviceActionInterface()
{
  /* Upon destruction shutdown action servers */
  as_move_ptp_.shutdown();
  as_move_lin_.shutdown();
  as_move_vel_.shutdown();
  as_move_servo_ptp_.shutdown();
}

/************************************************************************
 * ROS action callback functions
 ************************************************************************/

void SerialDeviceActionInterface::movePtpCB(const caros_control_msgs::MovePtpGoalConstPtr &goal)
{
  bool success = true;

  // parse speed, blend and accceleration
  const double speed_percent = goal->speed_percent;
  const double blend = goal->blend_radius;
  const double acceleration = goal->acceleration;

  // parse the joint configurations
  QAndSpeedAndBlendContainer_t res;
  res.clear();
  res.reserve(goal->joints.size());

  try
  {
    for (unsigned int index = 0; index < goal->joints.size(); index++)
    {
      rw::math::Q q(goal->joints[index].position.size());
      for (unsigned int i = 0; i < goal->joints[0].position.size(); i++)
      {
        q[i] = goal->joints[index].position[i];
      }

      res.push_back(std::make_tuple(q, speed_percent, blend));
    }
  }
  catch (const std::out_of_range& oor)
  {
    ROS_ERROR_STREAM("Container Out of Range error: " << oor.what());
  }

  // publish info to the console for the user
  ROS_INFO_STREAM("move_ptp: Executing, calling movePtp with goal q: " << std::get<0>(res.back()));

  // start executing the action
  success = movePtp(res);

  for (unsigned int index = 0; index < goal->joints[0].position.size(); index++)
    move_ptp_feedback_.joint_pose.push_back(static_cast<float>(0.0));

  // publish the feedback
  as_move_ptp_.publishFeedback(move_ptp_feedback_);

  if (success)
  {
    move_ptp_result_.result.error_code = 0;
    move_ptp_result_.result.error_msg = "";
    move_ptp_result_.result.succeeded = static_cast<unsigned char>(true);
    ROS_INFO("move_ptp: Succeeded!");
    // set the action state to succeeded
    as_move_ptp_.setSucceeded(move_ptp_result_);
  }
  else
  {
    move_ptp_result_.result.error_code = 1;
    move_ptp_result_.result.error_msg = "movePtp failed to execute";
    move_ptp_result_.result.succeeded = static_cast<unsigned char>(false);
    ROS_WARN("move_ptp: Failed");
    as_move_ptp_.setAborted(move_ptp_result_, "movePtp failed to execute");
  }
}

void SerialDeviceActionInterface::moveLinCB(const caros_control_msgs::MoveLinGoalConstPtr &goal)
{
  bool success = true;

  // parse speed and blend
  const double speed_percent = goal->speed_percent;
  const double blend = goal->blend_radius;

  // parse the target pose
  TransformAndSpeedAndBlendContainer_t res;
  res.clear();
  res.reserve(goal->tcp_pose.size());

  try
  {
    for (unsigned int index = 0; index < goal->tcp_pose.size(); index++)
    {
      rw::math::Vector3D<> pos(goal->tcp_pose[index].position.x, goal->tcp_pose[index].position.y,
                               goal->tcp_pose[index].position.z);

      rw::math::Quaternion<> quaternion(goal->tcp_pose[index].orientation.x, goal->tcp_pose[index].orientation.y,
                                        goal->tcp_pose[index].orientation.z, goal->tcp_pose[index].orientation.w);

      rw::math::Transform3D<> transform(pos, quaternion.toRotation3D());
      res.push_back(std::make_tuple(transform, speed_percent, blend));
    }
  }
  catch (const std::out_of_range& oor)
  {
    ROS_ERROR_STREAM("Container Out of Range error: " << oor.what());
  }

  // publish info to the console for the user
  ROS_INFO_STREAM("move_lin: Executing, calling moveLin with goal transform: " << std::get<0>(res.back()));

  // start executing the action
  success = moveLin(res);

  if (success)
  {
    move_lin_result_.result.error_code = 0;
    move_lin_result_.result.error_msg = "";
    move_lin_result_.result.succeeded = static_cast<unsigned char>(true);
    ROS_INFO("move_lin: Succeeded!");
    // set the action state to succeeded
    as_move_lin_.setSucceeded(move_lin_result_);
  }
  else
  {
    move_lin_result_.result.error_code = 1;
    move_lin_result_.result.error_msg = "moveLin failed to execute";
    move_lin_result_.result.succeeded = static_cast<unsigned char>(false);
    ROS_WARN("move_lin: Failed");
    as_move_lin_.setAborted(move_lin_result_, "moveLin failed to execute");
  }
}

void SerialDeviceActionInterface::moveVelCB(const caros_control_msgs::MoveVelGoalConstPtr &goal)
{
  move_vel_result_.result.error_code = 2;
  move_vel_result_.result.error_msg = "moveVel not implemented yet!";
  move_vel_result_.result.succeeded = static_cast<unsigned char>(false);
  ROS_WARN("move_vel: Failed, not implemented yet");
  as_move_vel_.setAborted(move_vel_result_, "moveVel not implemented yet!");
}

void SerialDeviceActionInterface::moveServoPtpCB(const caros_control_msgs::MoveServoPtpGoalConstPtr &goal)
{
  move_servo_ptp_result_.result.error_code = 2;
  move_servo_ptp_result_.result.error_msg = "moveServoPtp not implemented yet!";
  move_servo_ptp_result_.result.succeeded = static_cast<unsigned char>(false);
  ROS_WARN("move_servo_ptp: Failed, not implemented yet");
  as_move_servo_ptp_.setAborted(move_servo_ptp_result_, "moveServoPtp not implemented yet!");
}
