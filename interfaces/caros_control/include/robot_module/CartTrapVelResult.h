// Generated by gencpp from file robot_module/CartTrapVelResult.msg
// DO NOT EDIT!


#ifndef ROBOT_MODULE_MESSAGE_CARTTRAPVELRESULT_H
#define ROBOT_MODULE_MESSAGE_CARTTRAPVELRESULT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace robot_module
{
template <class ContainerAllocator>
struct CartTrapVelResult_
{
  typedef CartTrapVelResult_<ContainerAllocator> Type;

  CartTrapVelResult_()
    : position()
    , orientation()  {
    }
  CartTrapVelResult_(const ContainerAllocator& _alloc)
    : position(_alloc)
    , orientation(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _position_type;
  _position_type position;

   typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _orientation_type;
  _orientation_type orientation;




  typedef boost::shared_ptr< ::robot_module::CartTrapVelResult_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::robot_module::CartTrapVelResult_<ContainerAllocator> const> ConstPtr;

}; // struct CartTrapVelResult_

typedef ::robot_module::CartTrapVelResult_<std::allocator<void> > CartTrapVelResult;

typedef boost::shared_ptr< ::robot_module::CartTrapVelResult > CartTrapVelResultPtr;
typedef boost::shared_ptr< ::robot_module::CartTrapVelResult const> CartTrapVelResultConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::robot_module::CartTrapVelResult_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::robot_module::CartTrapVelResult_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace robot_module

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg'], 'robot_module': ['/home/verosim/dev/catkin_ws/devel/share/robot_module/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::robot_module::CartTrapVelResult_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robot_module::CartTrapVelResult_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robot_module::CartTrapVelResult_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
{
  static const char* value()
  {
    return "2ef4f32b212ce836da4e5b6b01a43767";
  }

  static const char* value(const ::robot_module::CartTrapVelResult_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x2ef4f32b212ce836ULL;
  static const uint64_t static_value2 = 0xda4e5b6b01a43767ULL;
};

template<class ContainerAllocator>
struct DataType< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
{
  static const char* value()
  {
    return "robot_module/CartTrapVelResult";
  }

  static const char* value(const ::robot_module::CartTrapVelResult_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\
#result definition\n\
float32[] position\n\
float32[] orientation\n\
";
  }

  static const char* value(const ::robot_module::CartTrapVelResult_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.position);
      stream.next(m.orientation);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct CartTrapVelResult_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::robot_module::CartTrapVelResult_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::robot_module::CartTrapVelResult_<ContainerAllocator>& v)
  {
    s << indent << "position[]" << std::endl;
    for (size_t i = 0; i < v.position.size(); ++i)
    {
      s << indent << "  position[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.position[i]);
    }
    s << indent << "orientation[]" << std::endl;
    for (size_t i = 0; i < v.orientation.size(); ++i)
    {
      s << indent << "  orientation[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.orientation[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // ROBOT_MODULE_MESSAGE_CARTTRAPVELRESULT_H
