// Generated by gencpp from file robot_module/CartLinTaskGoal.msg
// DO NOT EDIT!


#ifndef ROBOT_MODULE_MESSAGE_CARTLINTASKGOAL_H
#define ROBOT_MODULE_MESSAGE_CARTLINTASKGOAL_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <geometry_msgs/Pose.h>

namespace robot_module
{
template <class ContainerAllocator>
struct CartLinTaskGoal_
{
  typedef CartLinTaskGoal_<ContainerAllocator> Type;

  CartLinTaskGoal_()
    : target_pose()
    , desired_travel_time(0.0)
    , blend_radius(0.0)  {
    }
  CartLinTaskGoal_(const ContainerAllocator& _alloc)
    : target_pose(_alloc)
    , desired_travel_time(0.0)
    , blend_radius(0.0)  {
  (void)_alloc;
    }



   typedef std::vector< ::geometry_msgs::Pose_<ContainerAllocator> , typename ContainerAllocator::template rebind< ::geometry_msgs::Pose_<ContainerAllocator> >::other >  _target_pose_type;
  _target_pose_type target_pose;

   typedef float _desired_travel_time_type;
  _desired_travel_time_type desired_travel_time;

   typedef float _blend_radius_type;
  _blend_radius_type blend_radius;




  typedef boost::shared_ptr< ::robot_module::CartLinTaskGoal_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::robot_module::CartLinTaskGoal_<ContainerAllocator> const> ConstPtr;

}; // struct CartLinTaskGoal_

typedef ::robot_module::CartLinTaskGoal_<std::allocator<void> > CartLinTaskGoal;

typedef boost::shared_ptr< ::robot_module::CartLinTaskGoal > CartLinTaskGoalPtr;
typedef boost::shared_ptr< ::robot_module::CartLinTaskGoal const> CartLinTaskGoalConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::robot_module::CartLinTaskGoal_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace robot_module

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg'], 'robot_module': ['/home/reconcell/catkin_ws/devel/share/robot_module/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::robot_module::CartLinTaskGoal_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robot_module::CartLinTaskGoal_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robot_module::CartLinTaskGoal_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
{
  static const char* value()
  {
    return "3b0be358f4a523d714cf2f670ab02ed8";
  }

  static const char* value(const ::robot_module::CartLinTaskGoal_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x3b0be358f4a523d7ULL;
  static const uint64_t static_value2 = 0x14cf2f670ab02ed8ULL;
};

template<class ContainerAllocator>
struct DataType< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
{
  static const char* value()
  {
    return "robot_module/CartLinTaskGoal";
  }

  static const char* value(const ::robot_module::CartLinTaskGoal_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\
#goal definition\n\
geometry_msgs/Pose[] target_pose\n\
float32 desired_travel_time\n\
float32 blend_radius # CURRENTLY NOT USED. WILL BE USED LATER WHEN VIA POINT FUNCTIONALITY IS IMPLEMENTED\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Pose\n\
# A representation of pose in free space, composed of position and orientation. \n\
Point position\n\
Quaternion orientation\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Point\n\
# This contains the position of a point in free space\n\
float64 x\n\
float64 y\n\
float64 z\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Quaternion\n\
# This represents an orientation in free space in quaternion form.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
float64 w\n\
";
  }

  static const char* value(const ::robot_module::CartLinTaskGoal_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.target_pose);
      stream.next(m.desired_travel_time);
      stream.next(m.blend_radius);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct CartLinTaskGoal_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::robot_module::CartLinTaskGoal_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::robot_module::CartLinTaskGoal_<ContainerAllocator>& v)
  {
    s << indent << "target_pose[]" << std::endl;
    for (size_t i = 0; i < v.target_pose.size(); ++i)
    {
      s << indent << "  target_pose[" << i << "]: ";
      s << std::endl;
      s << indent;
      Printer< ::geometry_msgs::Pose_<ContainerAllocator> >::stream(s, indent + "    ", v.target_pose[i]);
    }
    s << indent << "desired_travel_time: ";
    Printer<float>::stream(s, indent + "  ", v.desired_travel_time);
    s << indent << "blend_radius: ";
    Printer<float>::stream(s, indent + "  ", v.blend_radius);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ROBOT_MODULE_MESSAGE_CARTLINTASKGOAL_H
