// Generated by gencpp from file robot_module/JointTrapVelResult.msg
// DO NOT EDIT!


#ifndef ROBOT_MODULE_MESSAGE_JOINTTRAPVELRESULT_H
#define ROBOT_MODULE_MESSAGE_JOINTTRAPVELRESULT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace robot_module
{
template <class ContainerAllocator>
struct JointTrapVelResult_
{
  typedef JointTrapVelResult_<ContainerAllocator> Type;

  JointTrapVelResult_()
    : joints()  {
    }
  JointTrapVelResult_(const ContainerAllocator& _alloc)
    : joints(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _joints_type;
  _joints_type joints;




  typedef boost::shared_ptr< ::robot_module::JointTrapVelResult_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::robot_module::JointTrapVelResult_<ContainerAllocator> const> ConstPtr;

}; // struct JointTrapVelResult_

typedef ::robot_module::JointTrapVelResult_<std::allocator<void> > JointTrapVelResult;

typedef boost::shared_ptr< ::robot_module::JointTrapVelResult > JointTrapVelResultPtr;
typedef boost::shared_ptr< ::robot_module::JointTrapVelResult const> JointTrapVelResultConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::robot_module::JointTrapVelResult_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::robot_module::JointTrapVelResult_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace robot_module

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg'], 'robot_module': ['/home/verosim/dev/catkin_ws/devel/share/robot_module/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::robot_module::JointTrapVelResult_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robot_module::JointTrapVelResult_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robot_module::JointTrapVelResult_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
{
  static const char* value()
  {
    return "e2a0e438b445b98def0f0ba6a2a85f58";
  }

  static const char* value(const ::robot_module::JointTrapVelResult_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xe2a0e438b445b98dULL;
  static const uint64_t static_value2 = 0xef0f0ba6a2a85f58ULL;
};

template<class ContainerAllocator>
struct DataType< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
{
  static const char* value()
  {
    return "robot_module/JointTrapVelResult";
  }

  static const char* value(const ::robot_module::JointTrapVelResult_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\
#result definition\n\
float32[] joints\n\
";
  }

  static const char* value(const ::robot_module::JointTrapVelResult_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.joints);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct JointTrapVelResult_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::robot_module::JointTrapVelResult_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::robot_module::JointTrapVelResult_<ContainerAllocator>& v)
  {
    s << indent << "joints[]" << std::endl;
    for (size_t i = 0; i < v.joints.size(); ++i)
    {
      s << indent << "  joints[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.joints[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // ROBOT_MODULE_MESSAGE_JOINTTRAPVELRESULT_H
