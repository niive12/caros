#ifndef CAROS_SERIAL_DEVICE_ACTION_INTERFACE_H
#define CAROS_SERIAL_DEVICE_ACTION_INTERFACE_H

#include <caros_control_msgs/MovePtpAction.h>
#include <caros_control_msgs/MoveLinAction.h>
#include <caros_control_msgs/MoveVelAction.h>
#include <caros_control_msgs/MoveServoPtpAction.h>
#include <caros_control_msgs/Status.h>

#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/VelocityScrew6D.hpp>
#include <rw/math/Wrench6D.hpp>

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>

#include <string>
#include <tuple>

/* Always publish the latest serial device state */
#define SERIAL_DEVICE_STATE_PUBLISHER_QUEUE_SIZE 1
#define SERIAL_DEVICE_ACTION_INTERFACE_SUB_NAMESPACE "caros_serial_device_action_interface"
#define ACTION_SERVER_NO_AUTOSTART false
#define ACTION_SERVER_AUTOSTART true

using TransformAndSpeedContainer_t = std::vector<std::tuple<const rw::math::Transform3D<>, const float>>;
using TransformAndSpeedAndBlendContainer_t =
    std::vector<std::tuple<const rw::math::Transform3D<>, const float, const float>>;
using QAndSpeedContainer_t = std::vector<std::tuple<const rw::math::Q, const float>>;
using QAndSpeedAndBlendContainer_t = std::vector<std::tuple<const rw::math::Q, const float, const float>>;

namespace caros
{
/**
 * @brief This is the serial device interface. It defines the (minimum) interface that a joint based robotic device
 *needs to implement.
 *
 * All interfaces use meters, radians, Newton and Newtonmeter as base units.
 * (linear joints in meters, rotary joints in radians.)
 * If a device does not support this please be very explicit in the documentation of your node about this.
 *
 * In ROS the namespace of the node is used and it is important that not two SerialDeviceActionInterface are running in
 *the
 *same namespace.
 */
class SerialDeviceActionInterface
{
 public:
  explicit SerialDeviceActionInterface(ros::NodeHandle nodehandle);

  virtual ~SerialDeviceActionInterface();

  void movePtpCB(const caros_control_msgs::MovePtpGoalConstPtr &goal);

  void moveLinCB(const caros_control_msgs::MoveLinGoalConstPtr &goal);

  void moveVelCB(const caros_control_msgs::MoveVelGoalConstPtr &goal);

  void moveServoPtpCB(const caros_control_msgs::MoveServoPtpGoalConstPtr &goal);

  //! @brief move robot on a linear Cartesian path (in meters)
  virtual bool moveLin(const TransformAndSpeedAndBlendContainer_t& targets)
  {
    return false;
  }
  //! @brief move robot from c-space point to c-space point (in radians for revolute joints / meters for linear)
  virtual bool movePtp(const QAndSpeedAndBlendContainer_t& targets)
  {
    return false;
  }
  //! @brief move robot from Cartesian point to Cartesian point (in meters) using a pose as target (requires invkin)
  virtual bool movePtpT(const TransformAndSpeedAndBlendContainer_t& targets) = 0;
  //! @brief move robot in a servoing fashion specifying joint velocity targets (in radians/sec for revolute joints /
  //! meters/sec for linear)
  virtual bool moveVelQ(const rw::math::Q& q_vel) = 0;
  //! @brief move robot in a servoing fashion specifying a velocity screw in tool coordinates (in meters/sec and
  //! radians/sec)
  virtual bool moveVelT(const rw::math::VelocityScrew6D<>& t_vel) = 0;
  /**
   * @brief move robot in a servoing fashion specifying a joint configuration (in radians for revolute joints and meters
   * for linear joints)
   * @note It is implementation specific whether the targets are being moved to individually, or just the last specified
   * target is chosen. Make sure to look at the specific implementation for the node you are using.
   */
  virtual bool moveServoQ(const QAndSpeedContainer_t& targets) = 0;
  /**
   * @brief move robot in a servoing fashion specifying a pose (in meters)
   * @note It is implementation specific whether the targets are being moved to individually, or just the last specified
   * target is chosen. Make sure to look at the specific implementation for the node you are using.
   */
  virtual bool moveServoT(const TransformAndSpeedContainer_t& targets) = 0;
  //! @brief hard stop the robot
  virtual bool moveStop() = 0;

 private:
  /**
   * @brief private default constructor.
   * This is declared as private to enforce deriving classes to call an available public constructor and enforce that
   * the ROS actions are properly initialised.
   */
  SerialDeviceActionInterface();

 protected:
  ros::NodeHandle nh_;  // NodeHandle instance must be created before the action servers.
  actionlib::SimpleActionServer<caros_control_msgs::MovePtpAction> as_move_ptp_;
  actionlib::SimpleActionServer<caros_control_msgs::MoveLinAction> as_move_lin_;
  actionlib::SimpleActionServer<caros_control_msgs::MoveVelAction> as_move_vel_;
  actionlib::SimpleActionServer<caros_control_msgs::MoveServoPtpAction> as_move_servo_ptp_;

  // create messages that are used to published feedback/result
  caros_control_msgs::MovePtpFeedback move_ptp_feedback_;
  caros_control_msgs::MovePtpResult move_ptp_result_;

  caros_control_msgs::MoveLinFeedback move_lin_feedback_;
  caros_control_msgs::MoveLinResult move_lin_result_;

  caros_control_msgs::MoveVelFeedback move_vel_feedback_;
  caros_control_msgs::MoveVelResult move_vel_result_;

  caros_control_msgs::MoveServoPtpFeedback move_servo_ptp_feedback_;
  caros_control_msgs::MoveServoPtpResult move_servo_ptp_result_;
};
}  // namespace caros

#endif  // CAROS_SERIAL_DEVICE_ACTION_INTERFACE_H
